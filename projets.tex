\documentclass[french]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fourier}
\usepackage[a4paper]{geometry}
\usepackage{hologo}
\usepackage[nospace]{varioref}
\usepackage{babel}
\usepackage[hidelinks]{hyperref}
\usepackage{glossaries}
\usepackage{cleveref}
%
\makeglossaries
\setacronymstyle{long-sc-short}
%
\newacronym{ca}{ca}{conseil d'administration}
\newacronym{ctan}{ctan}{comprehensive \TeX{} archive network}
\newacronym{ag}{ag}{assemblée générale}
\newacronym{ens}{ens}{école normale supérieure}
\newacronym[longplural=foires aux questions]{faq}{faq}{foire aux questions}
\newacronym{tug}{tug}{\TeX{} users group}
\newacronym{dante}{dante}{Deutschsprachige Anwendervereinigung TeX}
\newacronym{tp}{tp}{travaux pratiques}
\newacronym{lsr}{lsr}{LilyPond snippet repository}
%
\newcommand{\gut}{GUTenberg}
\newcommand{\package}[1]{\textsf{#1}}
\newcommand{\lien}[1]{\footnote{\url{#1}.}}
%
\begin{document}
\title{Projets pour une association GUTenberg renouvelée}
\author{Denis Bitouzé \and ...}
\maketitle

\begin{abstract}
  Ce document liste des projets, actuels ou futurs, que l'association \gut{}
  renouvelée pourrait porter.
\end{abstract}

\tableofcontents

\section{Introduction}
\label{sec:introduction}

L'association \gut{} des utilisateurs francophones de \TeX{} traverse une crise
tellement sérieuse que sa dissolution est envisagée. Pour éviter cela, il est
indispensable qu'un nouveau \gls{ca}, constitué de personnes motivées, soit élu
lors d'une prochaine \gls{ag} par un nombre significatif d'adhérents.

Or il nous apparaît que, pour que les utilisateurs francophones de \TeX{} soient
enclins à adhérer à l'association puis à participer à cette élection, il est
nécessaire qu'ils aient une idée des projets et services, actuellement fournis
ou à initier, que l'association \gut{} renouvelée pourrait porter.

C'est l'objet du présent document, produit collaborativement, que de lister de
tels projets avec, pour chacun d'eux, ses avantages et inconvénients.

\section{Projets actuels}
\label{sec:projets-actuels}

Cette section liste, sans prétendre à l'exhaustivité, les services rendus ou
projets menés actuellement par \gut{}.

\subsection{Site Internet}

\gut{} dispose d'un site Internet\lien{https://www.gutenberg.eu.org/}.
\begin{description}
\item[Avantages :] indispensable de nos jours.
\item[Inconvénients :]\
  \begin{itemize}
  \item difficile à alimenter ;
  \item difficile à rendre attrayant.
  \end{itemize}
\end{description}

\subsection{Liste \texttt{gut@ens.fr}}
\begin{description}
\item[Avantages :]\
  \begin{itemize}
  \item permet l'entraide (service également proposé par
    \nolinkurl{texnique.fr}, cf.~\vref{site-de-questions}) mais aussi des débats
    sur \TeX{} ;
  \item service connu et simple d'emploi, largement utilisé.
  \end{itemize}
\item[Inconvénients :]\
  \begin{itemize}
  \item actuellement hébergée sur un serveur de l'\gls{ens} sur lequel
    l'association n'a pas la main ;
  \item pas d'archives disponibles.
  \end{itemize}
\end{description}

\subsection{Liste \texttt{metafont@ens.fr}}
\begin{description}
\item[Avantages :] ?
\item[Inconvénients :]\
  \begin{itemize}
  \item actuellement hébergée sur un serveur de l'\gls{ens} sur lequel
    l'association n'a pas la main ;
  \item pas d'archives disponibles.
  \end{itemize}
\end{description}

\subsection{Cahiers \gut{}}
\begin{description}
\item[Avantages :]\
  \begin{itemize}
  \item revue de qualité ;
  \item articles utiles à beaucoup d'utilisateurs.
  \end{itemize}
\item[Inconvénients :]\
  \begin{itemize}
  \item très difficile à faire vivre ;
  \item chronophage.
  \end{itemize}
\end{description}

\subsection{Journées \gut{}}
\begin{description}
\item[Avantages :]\
  \begin{itemize}
  \item qualité des exposés ;
  \item convivialité ;
  \end{itemize}
\item[Inconvénients :]\
  \begin{itemize}
  \item trop souvent parisiennes ;
  \item manque d'exposés d'initiation à \hologo{(La)TeX}.
  \end{itemize}
\end{description}

\subsection{Site de questions et réponses}
\label{site-de-questions}

Le site francophone de questions et réponses dédiées à \hologo{(La)TeX},
analogue au site anglophone
\nolinkurl{tex.stackexchange}\lien{https://tex.stackexchange.com/}, est
\nolinkurl{texnique.fr}\lien{https://texnique.fr/}. Le propos de tels sites est
de fournir des solutions (et seulement des solutions\footnote{Les débats, même
  sur \TeX{}, y sont découragés.}) optimales aux questions que les utilisateurs
de \hologo{(La)TeX} peuvent se poser.

\begin{description}
\item[Avantages :]\
  \begin{itemize}
  \item très utile à la communauté ;
  \end{itemize}
\item[Inconvénients :]\
  \begin{itemize}
  \item manque de convivialité (mais ce n'est pas son propos) ;
  \item hébergé\footnote{Et techniquement essentiellement géré par Stefan
      Kottwitz : merci à lui !} sur un serveur du \gls{tug} germanophone
    \acrshort{dante}, sur lequel l'association n'a pas la main ;
  \item pas exempt de bugs, de limitations et possiblement de trous de sécurité
    car repose sur un système non maintenu (à remplacer par, par exemple,
    Question2Answer\lien{https://www.question2answer.org/}).
  \end{itemize}
\end{description}

\subsection{Soutien à des projets ponctuels}

Ce fut par exemple le cas du package \package{e-french}.

Un exemple de projet intéressant potentiellement la communauté francophone est
celui initié par Bernard Gaulle\footnote{Décédé en août 2007, il n'a pas pu
  l'achever.} et qui consistait à traduire en français les messages d'erreurs,
avertissements et autres émis lors de la compilation\footnote{Au moins ceux du
  moteur, bien sûr pas ceux de tous les packages et classes.} : ce pourrait être
utile y compris pour un public capable de comprendre l'anglais (penser par
exemple au message « ! Undefined control sequence. »).
\begin{description}
\item[Avantages :]\
  \begin{itemize}
  \item utile à la communauté.
  \end{itemize}
\item[Inconvénients :] ?
\end{description}

\section{Projets à initier}
\label{sec:projets-a-initier}

Cette section liste, sans prétendre à l'exhaustivité, les services à rendre ou
projets à mener par \gut{}.

\subsection{\texorpdfstring{\acrshort{faq}}{FAQ} francophone}

Les seules \glspl{faq} \LaTeX{} francophones actuelles :
\begin{itemize}
\item celle\lien{https://2001-faq-latex.fr/} de l'équipe Grappa ;
\item celle\lien{http://faqfctt.yojik.eu/datas/faqfr-20041111-3.00.alpha.pdf} du
  groupe \texttt{fr.comp.text.tex} ;
\end{itemize}
commencent à être obsolètes, notamment du fait de packages :
\begin{itemize}
\item obsolètes parfois signalés ;
\item récents et utiles pas signalés.
\end{itemize}

\begin{description}
\item[Avantages :]\
  \begin{itemize}
  \item très utile à la communauté ;
  \item peut être augmentée progressivement.
  \end{itemize}
\item[Inconvénients :]\
  \begin{itemize}
  \item projet souvent été envisagé, mais n'ayant jamais véritablement démarré.
  \end{itemize}
\end{description}

Le plus simple serait probablement de traduire en français la \gls{faq}
anglophone\lien{https://texfaq.org/} et de l'enrichir des deux francophones dont
on dispose\footnote{Possibles problèmes de droits pour celle de l'équipe
  Grappa : © Copyright 1997 Marie-Paule Kluth.}, à actualiser bien sûr (et qui
pourraient en retour enrichir l'anglophone).

\subsection{Journées de formation}

GUTenberg prendrait en charge les frais de déplacement, d'hébergement, etc. de
bénévoles qui assureraient des formations, notamment de niveau débutant, à Paris
mais aussi dans diverses villes de France (en s'appuyant sur des adhérents
relais).

\begin{description}
\item[Avantages :]\
  \begin{itemize}
  \item forte demande de la communauté ;
  \item permet d'atteindre des publics nouveaux.
  \end{itemize}
\item[Inconvénients :]\
  \begin{itemize}
  \item chronophage ;
  \item nécessite de disposer de locaux adaptés (salle(s) de conférence et de
    \gls{tp}).
  \end{itemize}
\end{description}

\subsection{Accueillir une conférence \texorpdfstring{\acrshort{tug}}{TUG} annuelle}

Le \gls{tug} américain, qui joue le rôle de \gls{tug} mondial, organise tous les
ans une conférence annuelle\lien{http://www.tug.org/meetings.html}. \gut{} en
a accueilli par le passé, mais ne l'a pas fait depuis longtemps.

\begin{description}
\item[Avantages :]\
  \begin{itemize}
  \item permet de donner de la visibilité à \gut{} ;
  \item motivant ;
  \item conférences données par les experts mondiaux de \TeX{}.
  \end{itemize}
\item[Inconvénients :]\
  \begin{itemize}
  \item très chronophage ;
  \item nécessite de disposer de locaux adaptés (salle(s) de conférence et de
    \gls{tp}) ;
  \item est souvent programmé en plein milieu de l'été, ce qui peut poser des
    problèmes pour bénéficier de locaux universitaires.
  \end{itemize}
\end{description}

\subsection{Contribution au \texorpdfstring{\acrshort{ctan}}{CTAN} et à \TeX{} Live}

\begin{itemize}
\item Le \acrshort{ctan}\lien{https://www.ctan.org/} est le lieu central de toutes
  sortes de matériaux liés à \TeX{}, notamment les divers classes et packages
  qui doivent s'y trouver pour pouvoir être intégrés aux distributions \TeX{}
  majeures. C'est donc un service essentiel pour la disponibilité et l'évolution
  de \TeX{}.
\item Pour pouvoir utiliser \TeX{}, on ne peut se passer d'une distribution
  \TeX{}. Il en existe actuellement deux majeures :
  \begin{itemize}
  \item la MiK\TeX{}\lien{https://miktex.org/}, essentiellement orientée Windows
    et portée par une seule personne : Christian Schenk ;
  \item la \TeX{} Live\lien{https://www.tug.org/texlive/}, plus généraliste car
    disponible sur la plupart des Unix (dont Linux), Mac~OS\footnote{Via la
      MacTeX : \url{https://www.tug.org/mactex/}.} et Windows, et plus pérenne
    car portée par le \gls{tug} mondial.
  \end{itemize}
\end{itemize}

\gut{} pourrait contribuer financièrement au \acrshort{ctan} et à la \TeX{}
Live, sous la forme d'un pourcentage des cotisations à fixer.

\begin{description}
\item[Avantages :]\
  \begin{itemize}
  \item pas chronophage ;
  \item très utile à la communauté ;
  \item participation à l'effort collectif.
  \end{itemize}
\item[Inconvénients :]\
  \begin{itemize}
  \item effort peu visible pour les adhérents car services déjà rendus
    « gratuitement ». Donc nécessite une communication claire.
  \end{itemize}
\end{description}

\subsection{Réservoir de microcodes}
\label{sec:reserv-de-micr}

L'idée est de proposer un site analogue au
\gls{lsr}\lien{http://lsr.di.unimi.it/LSR/html/whatsthis.html}.

Ce serait un endroit où les gens pourraient déposer des idées créatives pour
utiliser \hologo{(La)TeX} : ces idées seraient représentées par des sources
\hologo{(La)TeX} courts et précis qui montreraient une fonctionnalité
particulière ou un « hack », si possibles illustrés par des images parlantes.

\begin{description}
\item[Avantages :]\
  \begin{itemize}
  \item peu chronophage à l'usage ;
  \item utile à la communauté.
  \end{itemize}
\item[Inconvénients :]\
  \begin{itemize}
  \item chronophage au début (choix du logiciel le mieux adapté et mise en place
    du site accueillant ces morceaux de codes).
  \end{itemize}
\end{description}

\printglossary
\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
